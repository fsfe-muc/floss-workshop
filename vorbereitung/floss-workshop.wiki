= Struktur =

== TODO ==

  - [ ] Musterbeispiel finden (Bernhard)
        Wenn ich bis morgen nix besseres finde:
        https://pypi.python.org/pypi/random-cat/
        Ist MIT-Lizenz, funktioniert überall, ist schnell erklärt, und der
        Quellcode ist übersichtlich (https://github.com/gravmatt/random-cat/)


* Gegenseitiges kennenlernen, "Warum sind wir alle hier?" Was für Hintergründe
  haben wir?  Use. Study.Share.Improve kurz anreißen.

* Evtl. (je nachdem, was sich vorher ergibt) in Gruppen aufsplitten) (Enduser
  vs. Entwickler?) Zeitlich splitten?  Teilnehmer fragen?  Gibt es Teilnehmer,
  die eigene Projekte anfangen möchten?

* Konkrete Projekte vorstellen:

  * Teilnehmervorschläge?
  * Thunderbird
  * evtl. Android/F-Droid?
  * evtl. Python-Prog (um direkte Änderungen demonstrieren zu können)

Anhand eines Musterbeispiels, das stellvertretend für _jede_ freie Software
stehen kann.

* Eigene Erfahrungen mit Software-Problemen:

  * [ ] Hier Teilnehmer-Erfahrungen notieren

* Wie sieht das mit freier Software aus?


== Use ==

* Keine AGB (erwähnen: https://tldrlegal.com )
* Keine Einschränkungen


== Study ==

* Doku
* Wiki
* Aber auch: Quellcode
* Und: Kommuniktation mit Entwicklern (auch für andere Bereiche interessant)
     * IRC https://webchat.freenode.net
     * Mailingliste
* Und das Vertrauen: Technisch versiertere können den Code *auch* studieren.

Vor der Pause noch:

  * Finanzierung


== Pause ==

== Share + Improve ==

* Grundidee Git (nur: einfaches Teilen von Software)

* Issue Tracker

  * Bugs
  * Feature requests

* Übersetzungen beitragen

* CONTRIBUTE.txt

* Crowdsourcing von Software-Übersetzungen (parallelen zu Wikipedia, OSM und
  evtl. Galaxy-Klassifikationen ziehen; Kontrast zu User-Created-Content für
  Firmen; z.B. gehört jeder Facebook-Post Facebook)

* Demonstrieren: Eigene Veränderungen vornehmen (am Quellcode, der Doku und den
  Übersetzungefiles)


== Diskussion und Offenes Ende ==

* Interesse an weiteren Workshops?
* Leute bekannt, die an dem Thema interesse haben?
